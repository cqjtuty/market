<?php
namespace app\admin\controller;

use app\admin\controller\AdminBase;

class Users extends AdminBase
{
    public $user_id;

    public function _initialize()
    {
        parent::_initialize();
    }

    public function detail($user_id)
    {
        $users = model('index/User');
        $goods = model('index/Goods');
        $data = $users->select($user_id);
        $saled = $goods->where(['status' => 2, 'user_id' => $user_id])->select();
        $in_sale = $goods->where(['status' => 1, 'user_id' => $user_id])->select();

        $map['status'] = array('in', '3, 4');
        $map['user_id'] = $user_id;
        $nosale = $goods->where($map)->select();

        $saled = sizeof($saled);
        $in_sale = sizeof($in_sale);
        $nosale = sizeof($nosale);

        $goods->where(['user_id' => $user_id]);
        $goods_detail = $goods->paginate(5);
        $count = $goods->where(['user_id' => $user_id])->count();
        return view('', [

            'username' => $data[0]['username'],
            'last_login_time' => $data[0]['last_login_time'],
            'saled' => $saled,
            'in_sale' => $in_sale,
            'nosale' => $nosale,
            'list' => $goods_detail,
            'count' => $count

        ]);

    }

    function list() {
        $param = input('param.');
        $users = model('index/User');

        // 过滤
        switch (@$param['filter']) {
            case '1':
                $users->where(['status' => 1]);
                break;
            case '2':
                $users->where(['status' => 2]);
                break;
            case '0':
                $users->where(['status' => 0]);
                break;
            case '-1':
                $users->where(['status' => -1]);
                break;
            default:
                break;
        }

        // 排序
        switch (@$param['sort']) {
            case 'last_login':
                $users->order('last_login_time desc');
                break;
            case 'username':
                $users->order('username asc');
                break;
            case 'email':
                $users->order('email asc');
                break;
            default:
                $users->order('user_id asc');
                break;
        }

        if (@$param['username']) {
            $users->where(['username' => @$param['username']]);
        }

        $data = $users->paginate(9);
        return view('', ['list' => $data]);
    }

    /**
     * [nothrough description]
     * @Author 苦丁茶
     * @email  178183854@qq.com
     * @Date   2016-08-17
     *
     * 禁止登陆，除被删除用户以外都可使用此方法
     */
    public function nothrough($user_id)
    {
        $users = model('index/User');

        $s = $users->where(['user_id' => $user_id])->value('status');
        if ($s == 2 || $s == 1) {
            // 标记状态
            if (!$users->save(['status' => -1], ['user_id' => $user_id])) {
                return false;
            }

            return true;
        } else if ($s == -1) {
            if (!$users->save(['status' => 1], ['user_id' => $user_id])) {
                return false;
            }

            return true;
        }

    }

    /**
     * [through description]
     * @Author 苦丁茶
     * @email  178183854@qq.com
     * @Date   2016-08-17
     *
     * 通过验证，只可对未验证用户使用此方法
     */
    public function through($user_id)
    {
        $users = model('index/User');

        $s = $users->where(['user_id' => $user_id])->value('status');
        // 标记状态
        if (!$users->save(['status' => 2], ['user_id' => $user_id])) {
            return false;
        }

        return true;

    }

    /**
     * [recycle description]
     * @Author 苦丁茶
     * @email  178183854@qq.com
     * @Date   2016-08-17
     *
     * 删除用户，可对所有用户使用此方法
     */
    public function recycle($user_id)
    {
        $users = model('index/User');

        $s = $users->where(['user_id' => $user_id])->value('status');
        if ($s == 0) {
            // 标记状态
            if (!$users->save(['status' => 1], ['user_id' => $user_id])) {
                return false;
            }

            return true;
        } else {
            if (!$users->save(['status' => 0], ['user_id' => $user_id])) {
                return false;
            }

            return true;
        }

    }

}
