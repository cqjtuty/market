<?php 
namespace app\admin\controller;

use think\Db;
use think\Exception;
use think\Validate;
use app\admin\controller\AdminBase;

/**
* 
*/
class Feedback extends AdminBase
{
    
    public function _initialize()
    {
        parent::_initialize();
    }
    public function index()
    {
        $list = $this->getList();
        return view('',['list'=>$list]);
    }
    /**
     * 用户端的留言详情
     * @author cuibo weiai525@outlook.com at 2016-10-22
     * @return [type] [description]
     */
    public function detailByAdmin()
    {
        $message_id = input('get.id');
        return json($this->getDetail($message_id));
    }
    /**
     * 使用事务操作回复内容
     * @author cuibo weiai525@outlook.com at 2016-10-22
     * @param  [type] $data [description]
     * @return [type]       [description]
     */
    public function reply()
    {

        if (request()->isGet()) {
                    return view('');
        }
        $data = input('post.');
        // 字段验证
        $Validate = validate('admin/Feedback');
        if (!$Validate->scene('admin_reply')->check($data)) {
            return $this->error($Validate->getError());
        }
        $feedback_id = $data['feedback_id'];
        $feed_back = model('admin/Feedback');
        if ($feed_back->where('feedback_id',$feedback_id)->count() != 1) {
            return $this->error('留言id有误');
        }
        $this->dealContent($data['content']);
        // 启动事务
        Db::startTrans();
        try{
            $feedback_id = $data['feedback_id'];
            //$feed_back = model('admin/Feedback');
            if ($feed_back->where('feedback_id',$feedback_id)->update(['is_reply2'=>1,'is_reply1'=>0]) === false) {
                throw new Exception("Error Processing Request", 1);
            }
            $data['admin_id'] = $this->user_id;
            $feed_back_reply = model('admin/FeedbackReply');
            $feed_back_reply->data($data,true);
            if ($feed_back_reply->save() === false) {
                throw new Exception("Error Processing Request", 1);
            }
            // 提交事务
            Db::commit();   
            return $this->success('回复成功');
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            return $this->success('回复失败，请重试');
        }
    }

    public function getList()
    {
        $message_board = model('admin/Feedback');
        $data = $message_board->order('create_time', 'desc')->paginate(20);
        return $data;
    }
    /**
     * 获取留言详情，供内部调用
     * @author cuibo weiai525@outlook.com at 2016-10-22
     * @param  string $feedback_id 留言id
     * @return array             ['','reply'=>['']]
     */
    private function getDetail($feedback_id)
    {
        $message_board = model('admin/Feedback');
        $data = $message_board->where('feedback_id',$feedback_id)->find()->toArray();
        $message_board_reply = model('admin/FeedbackReply');
        $data2 = $message_board_reply->where('feedback_id',$feedback_id)->order('create_time')->select('content','create_time');
        $data['reply'] = $data2;
        return $data;
    }
     /**
     * 处理留言内容，目前为清除空格，可扩展其他需要处理的功能
     * @author cuibo weiai525@outlook.com at 2016-10-22
     * @param  string &$content 文本
     * @return void           
     */
    private function dealContent(&$content)
    {
        $content = trim($content);
    }
    /**
     * 关闭留言
     * @author cuibo weiai525@outlook.com at 2016-11-07
     * @return [type] [description]
     */
    public function closeFeedback()
    {
        $feedback_id = input('get.id');
        $feed_back = model('admin/Feedback');
        $res = $feed_back->where('feedback_id',$feedback_id)->update(['status'=>0]);
        if ($res === false) {
            return $this->success('关闭留言失败');
        }
        return $this->success('关闭留言成功');
    }
}
