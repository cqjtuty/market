<?php
namespace app\admin\controller;

use app\admin\controller\AdminBase;

class Site extends AdminBase
{
    public $user_id;

    public function _initialize()
    {
        parent::_initialize();
    }

    public function config()
    {
        return view('');
    }

    public function log()
    {
        if (input('time','')) {
            $time = input('time');
        }else{
            $time = date('Y-m-d');
        }
        $path = APP_PATH.'logs/'.$time.'.txt';
        if (!file_exists($path)) {
            $data = array();
        }else{
            $data = file_get_contents($path);
            $data = explode("\n", $data);
        }
        return view('',['list'=>$data]);
    }
}
