<?php
namespace app\admin\controller;

use think\Controller;
use think\db;

class Index extends Controller
{
    public $user_id;

    public function _initialize()
    {
        $user_id = is_login('user_id');
        if ($user_id) {
            $this->user_id = $user_id;
        } else {
            return $this->error('尚未登录，即将跳至登录页...', url('auth/login'));
        }
    }

    public function index()
    {
        $goods = model('index/Goods');
        $saled = $goods->where(['status' => '2'])->count();
        $in_sale = $goods->where(['status' => '1'])->count();
        $new_goods = $goods->where(['create_time' => ['gt', strtotime('-1 Week', time())]])->count();

        $goods_week = $goods->whereTime('create_time', 'week')->select();
        $number = sizeof($goods_week);
        $goods_w = [];
        for($x=($number-1); $x>=0; $x--)
        {
            $goods_w[$number-1-$x] = $goods_week[$x];
        }

        $users = model('index/User');
        $user_count = $users->count();

        $msg = model('Msg');
        $msg_week = $msg->whereTime('create_time', 'week')->select();
        $msg_number = sizeof($msg_week);
        $msg_w = [];
        for($x=($msg_number-1); $x>=0; $x--)
        {
            $msg_w[$msg_number-1-$x] = $msg_week[$x];
        }

        return view('', [
            'saled' => $saled,
            'in_sale' => $in_sale,
            'new_goods' => $new_goods,
            'user_count' => $user_count,
            'top10' => $msg_w,
            'list' => $goods_w,
        ]);
    }
}
