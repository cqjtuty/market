<?php
namespace app\admin\controller;

use think\Controller;
use think\Validate;
use think\Request;
use think\Hook;

class AdminBase extends Controller
{
    public $user_id;

    public function _initialize()
    {
        $user_id = is_login('user_id');
        if (!$user_id) {
            return $this->error('尚未登录，即将跳至登录页...', url('auth/login'));
        } 
        if(!$this->checkAccess($user_id)){
                $this->error("您没有访问权限！");
        }
        $this->user_id = $user_id;

    }
    private function checkAccess($user_id)
    {
        //如果用户id是1，则默认为超级管理员，无需判断
        if ($user_id == 1) {
            return true;
        }
            $request = Request::instance();
            $rules = [$request->module(),$request->controller(),$request->action()];
            $rules = [strtolower(implode('/', $rules))];

            return check_access($user_id,$rules,'or');
    }
    /**
     * 记录日志方法
     * @author cuibo weiai525@outlook.com at 2016-10-25
     * @param  string $message 主要包含在某一步操作中干了啥的的相关信息
     * @return [type]          [description]
     */
    public function doLog($message)
    {
        $result = Hook::listen('log',$message);
    }
}
