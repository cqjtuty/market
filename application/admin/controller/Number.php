<?php
namespace app\admin\controller;

use think\Controller;

class Number extends Controller
{
    public function number()
    {
        $goods = model('index/Goods');
        $goods_number = $goods->count();

        $users = model('index/User');
        $users_number = $users->count();

        $msg = model('Msg');
        $msg_number = $msg->count();

        return ['goods_number' => $goods_number,
                'users_number' => $users_number,
                'msg_number' => $msg_number,
            ];
    }
}
