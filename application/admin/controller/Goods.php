<?php
namespace app\admin\controller;

use app\admin\controller\AdminBase;

class Goods extends AdminBase
{

    public $user_id;

    public function _initialize()
    {
        parent::_initialize();
    }

    public function GoodsTypeManage()
    {
        return view('');
    }

    function list() {
        $param = input('param.');
        $goods = model('index/Goods');
        switch (@$param['filter']) {
            case '1':
                $goods->where(['status' => 1]);
                break;
            case '2':
                $goods->where(['status' => 2]);
                break;
            case '3':
                $goods->where(['status' => 3]);
                break;
            case '4':
                $goods->where(['status' => 4]);
                break;
            case '5':
                $goods->where(['status' => 5]);
                break;
            case '-1':
                $goods->where(['status' => -1]);
                break;
            case '0':
                $goods->where(['status' => 0]);
                break;
            default:
                break;
        }
        if (input('?get.title')) {
            $title = input('get.title');
            $str_len  = mb_strlen(trim($title));
            // 满足条件搜索用户
            if (mb_stripos($title, ',') == $str_len-1 && $str_len > 1) {
                $title = mb_substr($title, 0, $str_len -1);
                $users = model('index/User');
                $user_id = $users->where(['username' => $title])->field('user_id')->select();
                if ($user_id != array()) {
                    $ids = array_column($user_id, 'user_id');
                    $map['user_id'] = array('in', $ids);
                    $goods->where($map);
                } else {
                    $goods->where(['good_id' => 0]);
                }
            } else {
                $map['title'] = array('like', '%'.$title.'%');
                $goods->where($map);
            }
        }
        $data = $goods->paginate(9);
        return view('', ['list' => $data]);

    }

    public function addGoodsType()
    {
        return view('');
    }

    public function delGoodsType()
    {
        return view('');
    }
    /**
     * [recycle description]
     * @Author 苦丁茶
     * @email  178183854@qq.com
     * @Date   2016-08-17
     *
     * 删除货物，仅限在售货物使用该方法
     */
    public function recycle($good_id)
    {
        $goods = model('index/Goods');

        $s = $goods->where(['good_id' => $good_id])->value('status');
        if ($s != 0) {
            // 标记状态
            if (!$goods->save(['status' => 0], ['good_id' => $good_id])) {
                return false;
            }
            $message = "货物$good_id 设置status=0";
            $this->doLog($message);
            return true;
        } else {
            if (!$goods->save(['status' => 5], ['good_id' => $good_id])) {
                return false;
            }

            $message = "货物$good_id 设置status=5";
            $this->doLog($message);
            return true;
        }
    }

    /**
     * [through description]
     * @Author 苦丁茶
     * @email  178183854@qq.com
     * @Date   2016-08-17
     *
     * 审核货物，只有待审核，审核未通过货物才可使用本方法
     */
    public function through($good_id)
    {
        $goods = model('index/Goods');

        $s = $goods->where(['good_id' => $good_id])->value('status');
        // 标记状态
        if (!$goods->save(['status' => 1], ['good_id' => $good_id])) {
            return false;
        }
        $message = "货物$good_id 设置status=1";
        $this->doLog($message);
        return true;

    }

    /**
     * [nothrough description]
     * @Author 苦丁茶
     * @email  178183854@qq.com
     * @Date   2016-08-17
     *
     * 不予通过审核，只有待审核，在售的货物才可使用本方法
     */
    public function nothrough($good_id)
    {
        $validate = validate('index/Goods');
        $data = input('post.');
        if (!$validate->scene('not_through')->check($data)) {
            return $this->error($validate->getError());
        }
        $goods = model('index/Goods');
        $goods = $goods->where(['good_id' => $good_id])->find();
        if ($goods->getData('status') != 5) {
            return $this->error('只有待审核的物品才能通过审核');
        }
        // 标记状态
        if ($goods->save(['status' => -1,'examine_reason' => $data['examine_reason']], ['good_id' => $good_id])) {
            return $this->success('修改成功');
        }
        return $this->error('修改失败');
    }
    /**
     * 管理员下架物品，需要说明理由
     * @author cuibo weiai525@outlook.com at 2016-11-14
     * @param  integer $good_id 货物id
     * @return json|html         
     */
    public function notSell($good_id)
    {
        $validate = validate('index/Goods');
        $data = input('post.');
        if (!$validate->scene('not_sell')->check($data)) {
            return $this->error($validate->getError());
        }
        $goods = model('index/Goods');
        $goods = $goods->where(['good_id' => $good_id])->find();
        if ($goods->getData('status') != 1) {
            return $this->error('只有在售中的物品才能下架');
        }
        // 标记状态
        if ($goods->save(['status' => 6,'examine_reason' => $data['examine_reason']], ['good_id' => $good_id])) {
            return $this->success('修改成功');
        }
        return $this->error('修改失败');
    }

}
