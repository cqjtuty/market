<?php
namespace app\admin\controller;

use app\admin\controller\AdminBase;
use tree\Tree;
use think\Request;

class Category extends AdminBase
{
    public $user_id;

    public function _initialize()
    {
        parent::_initialize();
        $user_id = is_login('user_id');
        if ($user_id) {
            $this->user_id = $user_id;
        } else {
            return $this->error('尚未登录，即将跳至登录页...', url('auth/login'));
        }
    }

    public function list() {

        $cate = model('index/GoodsCategory');
        $cate = $cate->field('cate_id as id,parent_id as pid,title,description,status')->select();
        $cate = json_decode(json_encode($cate), 1);
        $tree = new Tree();
        $tree->init($cate);
        $list = [];
        $tree->getTreeList(0, $list);
        return view('', ['list' => $list]);
    }

    /**
     * [notuse description]
     * @Author 苦丁茶
     * @email  178183854@qq.com
     *
     * 二级商品状态修改
     */
    public function notuse($cate_id)
    {
        $cate = model('index/GoodsCategory');

        $s = $cate->where(['cate_id' => $cate_id])->value('status');
        if ($s == 0) {
            // 标记状态
            if (!$cate->save(['status' => 1], ['cate_id' => $cate_id])) {
                return false;
            }
            return true;

        } else {
            $goods = model('index/Goods');
            $map['cate_id'] = array('eq', $cate_id);
            $map['status'] = array('eq', 1);
            if ($goods->where($map)->select()) {
                return "修改失败！此分类下还有在售商品！！";
            }
            if (!$cate->save(['status' => 0], ['cate_id' => $cate_id])) {
                return false;
            }
            return true;

        }

    }
    /**
     * 添加分类（无限极分类）
     * @author cuibo weiai525@outlook.com at 2016-11-07
     */
    public function add()
    {
        if (request()->isGet()) {
            $cate = model('index/GoodsCategory');
            $cate = $cate->where('status', 1)->field('cate_id as id,parent_id as pid,title')->select();
            $cate = json_decode(json_encode($cate), 1);
            $tree = new Tree();
            $tree->init($cate);
            $list = [];
            $tree->getTreeList(0, $list);
            return view('', ['list' => $list]);
        }
        $data = Request::instance()->only(['title', 'description','parent_id','status']);
        $Validate = validate('Category');
        if (!$Validate->scene('add')->check($data)) {
            return $this->error($Validate->getError());
        }
        $cateModel = model('index/GoodsCategory');
        $cateModel->data($data, true);
        if (!$cateModel->save()) {
            return $this->error('系统错误');
        }
        return $this->success('添加成功');

    }
    /**
     * [addtype description]
     * @Author 苦丁茶
     * @email  178183854@qq.com
     *
     * 添加商品分类，包括一级分类和二级分类
     */
    public function addtype()
    {
        $cate = model('index/GoodsCategory');
        $type = input('post.');
        $data['parent_id'] = $type['parent_id'];

        // 在添加商品类别时可以顺便加上描述，只不过中间要用“：”分隔开
        $keyword = str_replace('：', ':', $type['title']);
        $arr = explode(':', $keyword);
        if (count($arr) == 1 && $arr[0] != '') {
            $data['title'] = $arr[0];
            $data['description'] = null;
            $data['status'] = 1;
            $r = $cate->insertGetId($data);
            if ($r) {
                return true;
            } else {
                return false;
            }

        } elseif (count($arr) > 1) {
            $data['title'] = $arr[0];
            $data['description'] = $arr[1];
            $data['status'] = 1;
            $r = $cate->insertGetId($data);
            if ($r) {
                return true;
            } else {
                return false;
            }

        }
    }

    /**
     * [changetype description]
     * @Author 苦丁茶
     * @email  178183854@qq.com
     *
     * 一级分类状态修改按钮文本动态显示
     */
    public function changetype()
    {
        $cate = model('index/GoodsCategory');
        $type = input('post.');
        $vo = $cate->where(['cate_id' => $type['cate_id']])->value('status');
        if ($vo == 0) {
            if ($cate->save(['status' => 1], ['cate_id' => $type['cate_id']])) {
                return true;
            } else {
                return false;
            }
        } else {
            $goods = model('index/Goods');
            $s = $cate->where(['parent_id' => $type['cate_id'], 'status' => 1])->field('cate_id')->select();
            $cate_id = array_column($s, 'cate_id');
            $cate_id[] = $type['cate_id'];
            $map['cate_id'] = array('in', $cate_id);
            $map['status'] = array('eq', 1);
            if ($goods->where($map)->select()) {
                return "修改失败！此分类下还有在售商品！！";
            }
            if ($cate->save(['status' => 0], ['cate_id' => $type['cate_id']])) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function revise()
    {
        $cate = model('index/GoodsCategory');
        $type = input('post.');
        if ($cate->save(['title' => $type['title'], 'description' => $type['description']], ['cate_id' => $type['cate_id']])) {
            return true;
        } else {
            return false;
        }
    }

}
