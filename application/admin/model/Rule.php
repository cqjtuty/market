<?php
namespace app\admin\model;

use think\Model;

class Rule extends Model
{

    /**
     * 规则
     *
     * rule_id     int     
     * name        varchar(15) 名称
     * route      varchar(255)  路由
     * status      tinyint    1 可用 0 不可用
     */

    protected $auto     = [];
    protected $insert   = ['status' => 1];
    protected $update   = [];

    // 关闭自动写入时间戳
    protected $autoWriteTimestamp = false;

    public function setRouteAttr($value)
    {
        return strtolower($value);
    }
}
