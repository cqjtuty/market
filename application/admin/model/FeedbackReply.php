<?php
namespace app\admin\model;

use think\Model;

class FeedbackReply extends Model
{

    /**
     * 留言板
     *
     * feedback_id      int     主键id
     * admin_id        int    管理员id 如为0则表示是用户回复给管理员
     * content         varchar(500) 回复内容
     * craeted_time    bigint  创建时间
     */

    protected $auto     = [];
    protected $insert   = [];
    protected $update   = [];
    protected $updateTime = false;
    public function getCreateTimeAttr($value)
    {
        return date('Y-m-d H:i',$value);
    }

}