<?php
namespace app\admin\model;

use think\Model;

class Feedback extends Model
{

    /**
     * 留言板
     *
     * feedback_id      int     主键id
     * user_id         int     用户id
     * title         varchar(25) 表提
     * content         varchar(500) 留言内容
     * is_reply1       用户回复管理员 是否有新回复 0 没有 1有，追加留言也将该字段设置为1 默认1
     * is_reply2       管理员回复用户 是否有新回复 0 没有 1有，追加留言也将该字段设置为1 默认0
     * status       1 开启 0关闭 关闭后，用户和管理员均不能再回复内容
     * create_time    bigint  创建时间
     * update_time    bigint  修改时间
     */

    protected $auto     = [];
    protected $insert   = [];
    protected $update   = [];

    public function getCreateTimeAttr($value)
    {
        return date('Y-m-d H:i:s',$value);
    }
    public function getNewReplyAttr($value)
    {
        if ($value == 1) {
            return '新回复';
        }else{
            return '暂无回复';
        }
    }

}