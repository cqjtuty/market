<?php
namespace app\admin\model;

use think\Model;

class Msg extends Model
{

    /**
     * 留言列表
     *
     * id           int     留言id
     * user_id      int     用户id
     * msg          int     留言内容
     * create_time  bigint  留言时间
     */

    protected $auto     = [];
    protected $insert   = ['create_time', 'user_id'];
    protected $update   = ['update_time'];

    protected function setUserIdAttr()
    {
        return is_login('user_id');
    }
}
