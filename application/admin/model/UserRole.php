<?php
namespace app\admin\model;

use think\Model;

class UserRole extends Model
{

    /**
     * 角色表
     *
     * role_id     int     角色id
     * user_id     int     用户id
     */

    protected $auto     = [];
    protected $insert   = [];
    protected $update   = [];

    // 关闭自动写入时间戳
    protected $autoWriteTimestamp = false;
}
