<?php
namespace app\admin\model;

use think\Model;

class Role extends Model
{

    /**
     * 角色表
     *
     * role_id     int     角色id
     * name        varchar(15) 名称
     * remark      varchar(50)  描述
     * status      tinyint    1 可用 0 不可用
     */

    protected $auto     = [];
    protected $insert   = ['status' => 1];
    protected $update   = [];
    // 关闭自动写入时间戳
    protected $autoWriteTimestamp = false;

    public function getStatusAttr($value)
    {
        return $value == 1?'正常':'禁用';
    }

}
