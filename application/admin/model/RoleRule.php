<?php
namespace app\admin\model;

use think\Model;

class RoleRule extends Model
{

    /**
     * 角色表
     *
     * role_id     int     角色id
     * rule_id     int     规则id
     * rule_route        varchar(255) 路由
     */

    protected $auto     = [];
    protected $insert   = [];
    protected $update   = [];

    // 关闭自动写入时间戳
    protected $autoWriteTimestamp = false;

}
