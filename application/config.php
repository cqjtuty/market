<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

return [

    // 密码盐
    'app_salt'  => 'r2IZgzxM',

    // 应用调试模式
    'app_debug'              => true,
    // 应用Trace
    'app_trace'              => true,
    // 应用状态
    'app_status'            => 'prod',
    'app_url'   =>'http://202.202.240.251/market/',
    // 伪静态后缀设置
    'url_html_suffix'=>'',

    // 扩展配置文件
    'extra_config_list'      => ['database', 'route', 'validate'],

    // 视图输出字符串内容替换
    'view_replace_str'  => [
        '__STATIC__'=>'/static',
        '__ROOT__' => '',
    ],

    // 是否自动写入时间戳字段
    'database'  => [
        'auto_timestamp' => true,
    ],

    //可用作加密解密密钥或盐值，必需为32位
    'app_key' => 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',

    // 邮箱配置
    'mail' => [
        'host' => 'smtp.163.com',
        'port' => 25,
        'username' => 'cqjtuty@163.com',
        'password' => 'cqjtuty2016',
        'encryption' => null,
    ],

    // thinkphp 验证码
    'captcha'  => [
        // 验证码字符集合
        //'codeSet'  => '2345678abcdefhijkmnpqrstuvwxyzABCDEFGHJKLMNPQRTUVWXY',
        'codeSet'  => '0123456789',
        // 验证码字体大小(px)
        'fontSize' => 18,
        // 是否画混淆曲线
        'useCurve' => false,
         // 验证码图片高度
        'imageH'   => 36,
        // 验证码图片宽度
        'imageW'   => 140,
        // 验证码位数
        'length'   => 4,
        // 验证成功后是否重置
        'reset'    => true
    ],

    // 站点跟目录 若无则不填 (http://host{/xxx}/index.html)
    // 'subroot'    => '/market',
    'subroot'   => '',

    // 用户上传文件存储路径
    'upload_path' => [
        'photo'     => '/uploads/photos',
    ],

    // 二手货配置项
    'goods' => [
        // 我的二手货每页显示数量
        'paginate'  => 16,
    ],

];
