<?php

/**
 * md5 密码加密算法
 * 需配合应用设置中 'app_salt' 加盐
 * @author 杨栋森 mutoe@foxmail.com at 2016-07-14
 *
 * @param  [string] $value [待加密字符串]
 * @return [string]        [md5摘要后的字符串]
 */
function password($value)
{
    $value .= config('app_salt');
    return md5($value);
}

/** 随机生成字符串
 * @author 杨栋森 mutoe@foxmail.com at 2016-07-19
 *
 * @param  integer $length 字符串长度
 * @return string
 */
function get_random_string($length = 32)
{
    $str = null;
    $strPol = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz";
    $max = strlen($strPol) - 1;

    for ($i = 0; $i < $length; $i++) {
        $str .= $strPol[rand(0, $max)]; //rand($min,$max)生成介于min和max两个数之间的一个随机整数
    }

    return $str;
}

/**
 * 登陆状态查询
 * @author 杨栋森 mutoe@foxmail.com at 2016-07-19
 *
 * @param  string  $return_type 返回的类型
 *                 email:       返回用户邮箱
 *                 username:    返回用户昵称
 *                 user_id:     返回用户id
 * @return boolean false: 未登录
 *                 null:  错误的参数
 */
function is_login($return_type = 'email')
{
    $email = session('email');
    if (!$email) {
        return false;
    }
    switch ($return_type) {
        case 'email':
            $result = $email;
            break;
        case 'username':
            $user = model('index/User');
            $result = $user->where(['email' => $email])->value('username');
            break;
        case 'user_id':
            $user = model('index/User');
            $result = $user->where(['email' => $email])->value('user_id');
            break;
        default:
            $result = null;
            break;
    }
    return $result;
}

/**
 * 根据user_id获取用户名
 * @author 杨栋森 mutoe@foxmail.com at 2016-07-28
 *
 * @param  integer $user_id
 * @return string  用户名
 */
function get_username($user_id = 0)
{
    if (!$user_id) {
        throw new \think\Excetpion('请输入正确的用户id');
    }
    $user = model('index/User');
    return $user->where(['user_id' => $user_id])->value('username');
}

/**

 * 获取商品类型

 * @Author 苦丁茶

 * @email  178183854@qq.com

 * @Date   2016-08-05

 */
function get_goodtype($cate_id = 0)
{
    $good = model('index/GoodsCategory');
    return $good->where(['cate_id' => $cate_id])->value('title');
}

/**
 * 计算时间差并返回动态时间字符串
 * 可显示的动态时间有秒、分钟、小时、天、个月、年
 * @author 杨栋森 mutoe@foxmail.com at 2016-07-23
 *
 * @param  int $timestamp 目标时间
 * @param  int $now_time  当前时间 default: 当前服务器时间
 * @return string  "n 秒/分钟/小时/天/个月/年"
 */
function get_time_diff($timestamp, $now_time = 0)
{
    if (!$now_time) {
        $now_time = time();
    }
    if ($timestamp < $now_time) {
        return 0;
    }
    $diff = $timestamp - $now_time;
    if ($diff < 60) {
        return floor($diff) . " 秒";
    }
    $diff /= 60;
    if ($diff < 60) {
        return floor($diff) . " 分钟";
    }
    $diff /= 60;
    if ($diff < 60) {
        return floor($diff) . " 小时";
    }
    $diff /= 24;
    if ($diff < 30) {
        return floor($diff) . " 天";
    }
    if ($diff >= 30 && $diff < 365) {
        $diff /= 30;
        return floor($diff) . " 个月";
    }
    $diff /= 365;
    return floor($diff) . " 年";
}

if (!function_exists('check_access')) {
    /**
     * 检查权限
     * @author cuibo weiai525@outlook.com at 2016-10-23
     * @param rules array  需要验证的规则列表,数组
     * @param user_id  int           认证用户的id
     * @param type string    如果为 'or' 表示满足任一条规则即通过验证;如果为 'and'则表示需满足所有规则才能通过验证
     * @return boolean           通过验证返回true;失败返回false
     */
    function check_access($user_id,$rules,$type)
    {
        $checkObj = new auth\Access();
        return $checkObj->check($user_id,$rules,$type);
    }
}
