<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

return [

    'index'     => 'index/index',
    'fuckie'    => 'common/fuckie',

    '[common]'  => [
        'refreshCaptcha'    => 'common/refreshCaptcha',
    ],

    '[admin]'   => [
        'number/number'        => 'admin/number/number',

        'site/config'          =>'admin/site/config',
        'site/log'             =>'admin/site/log',

        'feedback/reply'    => 'admin/feedback/reply',
        'feedback/index'    => 'admin/feedback/index',
        'feedback/detail'    => 'admin/feedback/detailByAdmin',

        'feedback/list'         => 'admin/feedback/list',
        'feedback/today'        => 'admin/feedback/today',
        'feedback/month'        => 'admin/feedback/month',
        'feedback/week'         => 'admin/feedback/week',
        'feedback/all'          => 'admin/feedback/all',

        'category/list'          => 'admin/category/list',
        'category/notuse'        => ['admin/category/notuse', ['method' => 'get']],
        'category/addtype'       => ['admin/category/addtype', ['method' => 'post']],
        'category/add'           => ['admin/category/add', ['method' => 'get|post']],
        'category/changetype'    => ['admin/category/changetype', ['method' => 'post']],
        'category/revise'        => ['admin/category/revise', ['method' => 'post']],

        'users/list'            => ['admin/users/list', ['method' => 'get']],
        'users/detail'          => 'admin/users/detail',
        'users/through'         => ['admin/users/through', ['method' => 'get']],
        'users/nothrough'       => ['admin/users/nothrough', ['method' => 'get']],
        'users/recycle'         => ['admin/users/recycle', ['method' => 'get']],

        'goods/recycle'         => ['admin/goods/recycle', ['method' => 'get']],
        'goods/through'         => ['admin/goods/through', ['method' => 'get']],
        'goods/nothrough'       => ['admin/goods/nothrough', ['method' => 'post']],
        'goods/addgoodstype'    => 'admin/goods/addGoodsType',
        'goods/delgoodstype'    => 'admin/goods/delGoodsType',
        'goods/goodstypemanage' => 'admin/goods/GoodsTypeManage',
        'goods/notsell'         => 'admin/goods/notSell',
        'goods'                 => 'admin/goods/list',

        'rbac/getrolelist' => ['admin/rbac/getRoleList', ['method' => 'get|post']],
        'rbac/addrole' => ['admin/rbac/addRole', ['method' => 'get|post']],
        'rbac/editrole' => ['admin/rbac/editRole', ['method' => 'get|post']],
        'rbac/deleterole' => ['admin/rbac/deleteRole', ['method' => 'get|post']],
        'rbac/getrule' => ['admin/rbac/getRule', ['method' => 'get|post']],
        'rbac/addrule' => ['admin/rbac/addRule', ['method' => 'get|post']],
        'rbac/editrule' => ['admin/rbac/editRule', ['method' => 'get|post']],
        'rbac/deleterule' => ['admin/rbac/deleteRule', ['method' => 'get|post']],
        'rbac/editrolerule' => ['admin/rbac/editRoleRule', ['method' => 'get|post']],
        'rbac/getrolerule' => ['admin/rbac/getRoleRule', ['method' => 'get|post']],
        'rbac/getroleuser' => ['admin/rbac/getRoleUser', ['method' => 'get|post']],
        'rbac/getuserrole' => ['admin/rbac/getUserRole', ['method' => 'get|post']],
        'rbac/edituserrole' => ['admin/rbac/editUserRole', ['method' => 'get|post']],

        ''                      => 'admin/index/index',
    ],

    '[auth]'    => [
        'login'         => 'auth/login',
        'register'      => 'auth/register',
        'logout'        => 'auth/logout',
        'autologin'     => 'auth/autoLogin',
        //'resetpassword' => 'auth/resetPassword',
        'reset'         => 'auth/clickResetUrl',
    ],

    '[goods]'   => [
        'list'          => 'goods/goodsList',
        ':good_id'      => ['goods/detail', ['method' => 'get'], ['good_id' => '\d+']],
        'add'           => ['goods/addGoods',['method' => 'get|post']],
        'modify'        => ['goods/updateField', ['method' => 'post']],
        'removepic'     => ['goods/removePhoto', ['method' => 'post']],
        'addpic'        => ['goods/addPhotos', ['method' => 'post']],
        'setthumb'      => ['goods/setThumb', ['method' => 'get']],
        'recycle'       => ['goods/recycle', ['method' => 'get']],
        'outrecycle'    => ['goods/outRecycle', ['method' => 'get']],
        'notsell'       => ['goods/notSell', ['method' => 'post']],
        'sell'       => ['goods/sell', ['method' => 'post']],
    ],

    '[user]'    => [
        'feedback/reply'    => 'feedback/reply',
        'feedback/index'    => 'feedback/index',
        'feedback/detail'    => 'feedback/detailByUser',
        'feedback/add'    => 'feedback/add',
        'setprotect'    => 'user/setProtect',
        'modifyimage'   => 'user/modifyImage',
        'modifypassword'=> 'user/modifyPassword',
        'getsellerinfo' => 'user/getSellerInfo',
        ''              => 'user/index',//将改路由放第一行匹配不到后面的路由
    ],

     '[test]'   => [
        'dictionary'    => 'test/dictionary',
        'test'          => 'test/test',
    ],

     '[api]'    => [
        'croppic'       => 'api/croppic/crop',
        'removepic'     => 'api/croppic/removepic',
        'upload/imagetmp'     => 'api/upload/imagetmp',
    ],

];
