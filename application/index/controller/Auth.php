<?php
namespace app\index\controller;

use app\index\model\User;
use mail\Mail;
use think\Cache;
use think\Controller;
use think\Validate;

class Auth extends Controller
{
    public function _initialize()
    {
        // 如果已经登陆并且不是请求注销
        if (!request()->isAjax() && is_login() && request()->path() != "auth/logout") {
            $this->redirect('user/index');
        }
    }

    public function login()
    {
        if (request()->isGet()) {
            return view('');
        }
        if (request()->isPost()) {
            $data = input('post.');

            // 验证字段合法性
            $validate = validate('User');

            if ($validate->scene('login')->check($data)) {
                $user = model('User');
                // 获取用户密码
                $password = $user->where(['email' => $data['email']])->value('password');
                if (!$password) {
                    $this->error("邮箱不存在");
                }

                // 核对密码
                if ($password == password($data['email'] . $data['password'])) {

                    // 执行登陆
                    $remember = isset($data['remember_me']) && $data['remember_me'];
                    $this->doLogin($data['email'], $remember);
                    $redirect_url = $this->getRedirectUrl();
                    return $this->success('登陆成功，即将跳转回首页...', $redirect_url);
                } else {
                    return $this->error("密码错了");
                }
            } else {
                return $this->error($validate->getError());
            }
        }
    }

    /**
     * 根据 remember_token 自动登录
     * @author 杨栋森 mutoe@foxmail.com at 2016-07-19
     *
     * @return ajax_return
     */
    public function autoLogin()
    {
        if (cookie('email') && cookie('remember_token')) {
            $user = model('User');
            $email = cookie('email');
            // 获取登陆口令
            $token = $user->where(['email' => $email])->value('remember_token');
            if ($token == cookie('remember_token')) {
                // 验证通过 开始登陆
                $this->doLogin($email, true);
                return $this->success(is_login('username'));
            }
        }
        return $this->error('自动登录失败');
    }

    /**
     * 执行登陆
     * @author 杨栋森 mutoe@foxmail.com at 2016-07-19
     *
     * @param  emai    $email           待写入用户email
     * @param  boolean $remember_me     是否记住用户
     */
    private function doLogin($email, $remember_me = false)
    {
        // 登陆状态写入
        session('email', $email);
        // 写入上次登陆time 和 ip
        $user = model('User');
        $user->save([
            'last_login_time' => time(),
            'last_login_ip' => request()->ip(),
        ], ['email' => $email]);

        // 记住登陆状态
        if ($remember_me) {
            $new_string = get_random_string(32);
            $user->save(['remember_token' => $new_string], ['email' => $email]);
            cookie('email', $email, ['expire' => 3600 * 24 * 30 * 12]);
            cookie('remember_token', $new_string, ['expire' => 3600 * 24 * 30]);
        }
    }

    public function register()
    {
        if (request()->isGet()) {
            return view('');
        }
        if (request()->isPost()) {
            $data = input('post.');

            // 验证字段合法性
            $validate = validate('User');

            if ($validate->scene('register')->check($data)) {
                $user = model('User');
                $user->data([
                    'email' => $data['email'],
                    'username' => $data['username'],
                    'password' => password($data['email'] . $data['password']),
                ]);
                if ($user->save()) {
                    session('email', $data['email']); // 登陆状态写入
                    return $this->success('注册成功，即将跳转回首页...', url('index/index'));
                } else {
                    return $this->error($user->getError());
                }
            } else {
                return $this->error($validate->getError());
            }
        }
    }

    /**
     * 检测email是否重复
     * @author 杨栋森 mutoe@foxmail.com at 2016-07-14
     *
     * @param  string $email 待检测email
     * @return boolean true:通过验证 false:email已存在
     */
    public function check_email_legal($email = "")
    {
        $user = model('User');
        $result = $user->where(['email' => $email])->value('id');
        return $result;
    }

    /**
     * 检测用户名是否重复
     * @author 杨栋森 mutoe@foxmail.com at 2016-07-14
     *
     * @param  string $username 待检测用户名
     * @return boolean true:用户名已存在 false:用户名不存在
     */
    public function check_username_legal($username = "")
    {
        $user = model('User');
        $result = $user->where(['username' => $username])->value('id');
        return $result;
    }

    /**
     * 注销登陆
     * 清空session和cookie的remember_token
     * @author 杨栋森 mutoe@foxmail.com at 2016-07-19
     */
    public function logout()
    {
        session('email', null);
        cookie('remember_token', null);

        return $this->success('注销成功，正在返回首页', url('index'));
    }

    /**
     * 请求重置密码接口，用户主动请求
     */
    public function resetPassword()
    {
        if (request()->isGet()) {
            return view('');
        }
        $cache_prefix = 'resetmail_';
        $data = input('post.');
        $validate = validate('User');
        if (!$validate->scene('reset_password_mail')->check($data)) {
            return $this->error($validate->getError());
        }
        $token = md5(time() . config('app_salt'));
        Cache::set($cache_prefix . $data['email'], $token, 3600 * 24);
        $url = config('app_url') . url('auth/clickResetUrl') . '?token=' . $token . '&email=' . $data['email'];
        Mail::send('mail/resetpassword', ['url' => $url], [
            'subject' => '重置密码',
            'from' => ['address' => config('mail')['username'], 'name' => '天佑二手市场'],
            'to' => $data['email'],
        ]);
        return $this->success('已发送重置密码链接到您的邮箱，请登录邮箱查看');
    }

    /**
     * 点击邮箱链接重置密码接口
     */
    public function clickResetUrl()
    {
        if (request()->isGet()) {
            return view('');
        }
        $data = input('post.');
        $validate = validate('User');
        if (!$validate->scene('click_reset_url')->check($data)) {
            return $this->error($validate->getError());
        }
        $cache_prefix = 'resetmail_';
        $token = input('token');
        $email = input('email');
        if (!$token == Cache::get($cache_prefix . $email)) {
            return $this->error('链接失效');
        }
        // TODO:重置密码
        $result = User::where('email', $email)->update(['password' => password($email . $data['password'])]);
        if ($result === false) {
            return $this->error('重置密码出错: 新密码写入失败');
        }
        Cache::rm($cache_prefix . $email);
        return $this->success('密码重置成功');
    }
    /**
     * 得到登陆后的重定向url
     * @author cuibo weiai525@outlook.com at 2016-10-24
     * @return string url
     */
    private function getRedirectUrl()
    {
        $request = request();
        if ($request->has('redirect_url','get')) {
            return $request->get('redirect_url');
        }
        $referer = $request->instance()->header('referer');
        if ($referer) {
            return $referer;
        }
        return url('index/index');

    }

}
