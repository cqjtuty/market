<?php
namespace app\index\controller;

use think\Controller;
/**
 * index模块controller基类，原则该模块下的所有controller都应继承该类
 */
class IndexBase extends Controller
{
    public $user_id;
    public $except_action = [];
    
    public function _initialize()
    {
        $user_id = is_login('user_id');
        if (!$user_id) {
            // 允许无需登陆的页面跳过登陆检测
            if (!in_array(request()->action(), $this->except_action)) {
                return $this->error('尚未登录，即将跳至登录页...', '/auth/login?redirect_url='.request()->instance()->url(true));
            }
        }
        @$this->user_id = $user_id;
    }
}
