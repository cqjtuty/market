<?php 
namespace app\index\controller;

use think\Db;
use think\Exception;
use think\Validate;
use app\index\controller\IndexBase;

/**
* 
*/
class Feedback extends IndexBase
{
    
    public function _initialize()
    {
        parent::_initialize();
    }
    public function index()
    {
        $list = $this->getList();
        return view('',['list'=>$list]);
    }
    /**
     * 用户端的留言详情
     * @author cuibo weiai525@outlook.com at 2016-10-22
     * @return [type] [description]
     */
    public function detailByUser()
    {
        $message_id = input('get.id');
        return json($this->getDetail($message_id));
    }
    /**
     * 用户添加留言
     * @author cuibo weiai525@outlook.com at 2016-10-22
     */
    public function add()
    {
        if (request()->isGet()) {
                    return view('');
        }
        $data = input('post.');
        // 字段验证
        $Validate = validate('admin/Feedback');
        if (!$Validate->scene('add')->check($data)) {
            return $this->error($Validate->getError());
        }
        unset($data['captcha']);
        $this->dealContent($data['content']);
        if (!isset($data['title']) || !$data['title']) {
            $data['title'] = mb_substr($data['content'], 0,15);
        }
        $data['user_id'] = $this->user_id;
        $message_board = model('admin/Feedback');
        $data['is_reply1'] = 1;
        $message_board->data($data,true);
        if(!$message_board->save()){
            return $this->error('系统错误，请重试！');
        }
        return $this->success('留言成功');
    }
    /**
     * 使用事务操作回复内容
     * @author cuibo weiai525@outlook.com at 2016-10-22
     * @param  [type] $data [description]
     * @return [type]       [description]
     */
    public function reply()
    {

        if (request()->isGet()) {
                    return view('');
        }
        $data = input('post.');
        // 字段验证
        $Validate = validate('admin/Feedback');
        if (!$Validate->scene('user_reply')->check($data)) {
            return $this->error($Validate->getError());
        }
        $feedback_id = $data['feedback_id'];
        $feed_back = model('admin/Feedback');
        if ($feed_back->where('feedback_id',$feedback_id)->where('user_id',$this->user_id)->count() != 1) {
            return $this->error('留言id有误');
        }
        unset($data['captcha']);
        $this->dealContent($data['content']);
        // 启动事务
        Db::startTrans();
        try{
            $feedback_id = $data['feedback_id'];
            //$feed_back = model('admin/Feedback');
            if ($feed_back->where('feedback_id',$feedback_id)->where('user_id',$this->user_id)->update(['is_reply1'=>1]) === false) {
                throw new Exception("Error Processing Request", 1);
            }
            $data['admin_id'] = 0;
            $feed_back_reply = model('admin/FeedbackReply');
            $feed_back_reply->data($data,true);
            if ($feed_back_reply->save() === false) {
                throw new Exception("Error Processing Request", 1);
            }
            // 提交事务
            Db::commit();   
            return $this->success('回复成功');
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            return $this->success('回复失败，请重试');
        }
    }

    public function getList()
    {
        $message_board = model('admin/Feedback');
        $data = $message_board->where('user_id',$this->user_id)->order('create_time', 'desc')->select();
        return $data;
    }
    /**
     * 获取留言详情，供内部调用
     * @author cuibo weiai525@outlook.com at 2016-10-22
     * @param  string $feedback_id 留言id
     * @return array             ['','reply'=>['']]
     */
    private function getDetail($feedback_id)
    {
        $message_board = model('admin/Feedback');
        $data = $message_board->where('feedback_id',$feedback_id)->where('user_id',$this->user_id)->find();
        if (!is_object($data)) {
            return [];
        }
        $data->save(['is_reply2' => 0],['feedback_id' => $feedback_id]);
        $data = $data->toArray();
        $message_board_reply = model('admin/FeedbackReply');
        $data2 = $message_board_reply->where('feedback_id',$feedback_id)->order('create_time')->select('content','create_time');
        $data['reply'] = $data2;
        return $data;
    }
     /**
     * 处理留言内容，目前为清除空格，可扩展其他需要处理的功能
     * @author cuibo weiai525@outlook.com at 2016-10-22
     * @param  string &$content 文本
     * @return void           
     */
    private function dealContent(&$content)
    {
        $content = trim($content);
    }
}
