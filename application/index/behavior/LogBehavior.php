<?php 
namespace app\index\behavior;

class LogBehavior
{
    public function run(&$params)
    {
        /*Log::init([
            'type'  =>  'File',
            'path'  =>  APP_PATH.'logs/admin_log/',
            'apart_level'   =>  ['admin_log'],
            'level' => ['admin_log'],
        ]);*/
        //trace('错误信息','admin_log');
        $user_id = is_login('user_id');
        $user = get_username($user_id);
        $time = date('Y-m-d H:i:s');
        $do = $params;//传入消息“干了啥”
        $str = "$user_id $user $time $do";
        $this->log($str);
    }
    private function log($string)
    {
        $path = APP_PATH.'logs/';
        if (!file_exists($path)) {
            mkdir($path);
        }
        $name = date('Y-m-d').'.txt';
        $fileName = $path.$name;
        $fp  = fopen( $fileName, 'a' );
        fwrite($fp, $string."\n");
        fclose($fp);
    }
}


