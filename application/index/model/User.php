<?php
namespace app\index\model;

use think\Model;

class User extends Model
{
    protected $auto     = [];
    protected $insert   = ['status' => 1, 'create_ip', 'create_time'];
    protected $update   = ['update_time'];

    protected function setCreateIpAttr()
    {
        return request()->ip();
    }

    public function getStatusAttr($value)
    {
    	$status = [
    		1 	=> '身份未验证',
    		2 	=> '已验证',
    		0 	=> '用户被删除',
    		-1 	=> '禁止登陆',
    	];
    	return $status[$value];
    }

}
