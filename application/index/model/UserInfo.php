<?php
namespace app\index\model;

use think\Model;

class UserInfo extends Model
{

    /**
     * 用户信息表
     *
     * user_id      int     用户id
     * area         tinyint 地区
     *                      1: 南岸校区 2: 双福校区
     * ecardno      bigint  一卡通号
     * name         text    姓名
     * phone        bigint  手机号
     * address      varchar 地址
     * image        varchar 头像路径
     * protect      tinyint 信息保护模式
     *                      1: 不保护 2: 一般模式 3: 一般模式增强 4: 严格保护
     */

    protected $auto     = [];
    protected $insert   = ['area' => 1];
    protected $update   = [];

    public function getAreaAttr($value)
    {
        $status = [1 => '南岸', 2 => '双福'];
        return @$status[$value];
    }

    public function getProtectAttr($value)
    {
        $status = [1 => '不保护', 2 => '一般模式', 3 => '增强模式', 4 => '严格保护'];
        return $status[$value];
    }

    public function getImageAttr($value)
    {
        return config('subroot') . '/uploads/avatar/' . $value;
    }

}
