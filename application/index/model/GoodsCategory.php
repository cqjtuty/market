<?php
namespace app\index\model;

use think\Model;

class GoodsCategory extends Model
{

    /**
     * 货物分类表
     *
     * cata_id      int     分类id
     * parent_id    int     父级分类id
     * title        varchar 分类名
     * description  text    分类描述
     * status       tinyint 状态 1: 启用, 0: 禁用
     */

    protected $auto     = [];
    protected $insert   = ['status' => 1];
    protected $update   = [];
    
    // 关闭自动写入时间戳
    protected $autoWriteTimestamp = false;
    public function getStatusAttr($value)
    {
        $status = [
            1   => '启用',
            0   => '禁用',
        ];
        return $status[$value];
    }
}
