<?php
namespace app\index\model;

use think\Model;

class Goods extends Model
{

    /**
     * 货物表
     *
     * good_id      int     货物id
     * cate_id      int     货物分类id
     * user_id      int     用户id
     * area         tinyint 地区
     *                      1: 南岸校区 2: 双福校区
     * title        varchar 货物名称
     * description  text    货物描述
     * create_time  bigint  创建时间(timestamp)
     * update_time  bigint  修改时间(timestamp)
     * recycle_time bigint  加入回收站的时间(timestamp)，后期自动删除回收站超时物品
     * status       tinyint 货物状态
     *                      1: 在售 2: 已售出 3: 卖家下架 4: 超时下架 5: 待审核 -1: 审核未通过 0: 回收站 6:管理员下架 
     * type         tinyint 处理类型
     *                      1: 出售 2: 租赁
     * deadline     bigint  下架时间(timestamp)
     * hits         int     点击量
     * thumb        varchar 缩略图地址
     * price        int     标价
     * examine_reason varchar   管理员审核不通过的原因，管理员下架
     * bargain      tinyint 可议价
     *                      0: 不可议价 1: 可小刀 2: 可大刀
     * expand       text    拓展字段 存放json格式字符串
     *                      photos: 存放货物展示图片uri数组
     *                      photos_count: 当前展示图片数量
     *                      reject_msg: 二手货未通过审核原因
     */

    protected $auto     = [];
    protected $insert   = ['status' => 1, 'hits' => 0, 'create_time', 'user_id'];
    protected $update   = ['update_time'];

    protected function setUserIdAttr()
    {
        return is_login('user_id');
    }

    /**
     * 设置下架日期 以天为单位
     * @author 杨栋森 mutoe@foxmail.com at 2016-07-27
     *
     * @param  int $value 上架天数
     */
    public function setDeadlineAttr($value)
    {
        $now = time();
        if (!$value) {
            return $this->deadline;
        }
        return $now + $value * 24 * 3600;
    }

    /**
     * tp5 获取器 将拓展字段 expand 从 json 转化为 array
     * 使用时只需 {$vo.expand.photos_count} 即可
     * @author 杨栋森 mutoe@foxmail.com at 2016-07-25
     */
    public function getExpandAttr($value)
    {
        return json_decode($value, true);
    }

    public function getBargainAttr($value)
    {
        $result = [0 => '不二价', 1 => '可小刀', 2 => '随便砍'];
        return $result[$value];
    }

    public function getAreaAttr($value)
    {
        $result = [1 => '南岸校区', 2 => '双福校区'];
        return $result[$value];
    }

    /**
     * [getAreaAttr description]
     * @Author 苦丁茶
     * @email  178183854@qq.com
     * @Date   2016-08-03
     * 获取货物当前状态
     */
    public function getStatusAttr($value)
    {
        $result = [1 => '在售', 2 => '已售出', 3 => '卖家下架', 4 => '超时下架', 5 => '待审核', -1 => '审核未通过', 0 => '已删除', 6 => '管理员下架'];
        return @$result[$value];
    }
    /**
     * 将价格格式化为两位小数
     * @author cuibo weiai525@outlook.com at 2016-11-09
     * @param   $value [description]
     * @return [type]        [description]
     */
    public function getPriceAttr($value)
    {
        return sprintf ( "%0.2f" , $value );
    }

}
