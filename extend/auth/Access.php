<?php 
namespace auth;

use app\index\model\User;
use think\Db;

class Access
{
    /**
      * 检查权限
      * @param rules array  需要验证的规则列表,支持逗号分隔的权限规则或索引数组
      * @param user_id  int           认证用户的id
      * @param type string    如果为 'or' 表示满足任一条规则即通过验证;如果为 'and'则表示需满足所有规则才能通过验证
      * @return boolean           通过验证返回true;失败返回false
     */
    public function check($user_id,$rules,$type)
    {
        //获取加入系统权限的规则
        $allRules = db('rule')->where('status',1)->select();
        $arr = [];
        foreach ($allRules as $key => $value) {
            $arr[] = $value['route'];
        }
        //得到存在于系统权限的规则，在后面进行验证
        $rules = array_intersect($rules,$arr);
        //系统权限为空，或需要验证的规则不存在于系统权限，则不需要验证返回true
        if (empty($arr) || empty($rules)) {
            return true;
        }
        $data = Db::query('select * from ts_role_rule where role_id in (select role_id from ts_user_role where user_id='.$user_id.')');
        if ($data === false || empty($data) ) {
            return false;
        }
        $list = [];
        foreach ($data as $key => $value) {
            $list[] = strtolower($value['rule_route']);
        }
        //计算交集，不为空则表示有一条规则满足要求
        if ($type == 'or' && !empty(array_intersect($rules,$list))) {
            return true;
        }

        if (empty(array_diff($rules, $list))) {
            return true;
        }
        return false;

    }
}
