<?php 
namespace crypt;

/**
* 加密
*/
class Mcrypt
{
    
   public static function enAES($plaintext)
   {
        $key  =  pack ( 'H*' , config('app_key') );// 显示 AES-128, 192, 256 对应的密钥长度：16，24，32 字节。
        //为 CBC 模式创建随机的初始向量
        $iv_size  =  mcrypt_get_iv_size ( MCRYPT_RIJNDAEL_128 ,  MCRYPT_MODE_CBC );
        $iv  =  mcrypt_create_iv ( $iv_size ,  MCRYPT_RAND );//创建和 AES 兼容的密文（Rijndael 分组大小 = 128）仅适用于编码后的输入不是以 00h 结尾的
        //（因为默认是使用 0 来补齐数据）
        $ciphertext  =  mcrypt_encrypt ( MCRYPT_RIJNDAEL_128 , $key ,$plaintext , MCRYPT_MODE_CBC , $iv );
        //将初始向量附加在密文之后，以供解密时使用
        $ciphertext  =  $iv  .  $ciphertext ;
        //对密文进行 base64 编码
        $ciphertext_base64  =  base64_encode ( $ciphertext );
        return $ciphertext_base64;
        
   }
   public static function deAES($ciphertext)
   {
        $key  =  pack('H*' , config('app_key'));
        $ciphertext_dec  =  base64_decode ($ciphertext);
        if (!$ciphertext_dec) {
            return false;
        }
        //初始向量大小，可以通过 mcrypt_get_iv_size() 来获得
        $iv_size  =  mcrypt_get_iv_size ( MCRYPT_RIJNDAEL_128 ,  MCRYPT_MODE_CBC );
        $iv_dec  =  substr ( $ciphertext_dec ,  0 ,  $iv_size );
        //获取除初始向量外的密文
        $ciphertext_dec  =  substr ( $ciphertext_dec ,  $iv_size );
        //可能需要从明文末尾移除 0
        $plaintext_dec  =  mcrypt_decrypt ( MCRYPT_RIJNDAEL_128 ,  $key ,$ciphertext_dec ,  MCRYPT_MODE_CBC ,  $iv_dec );
        return $plaintext_dec;
   }
}
