<?php
namespace mail;

use think\View;
use Swift_SmtpTransport;
use Swift_Mailer;
use Swift_Message;

/**
* 基于swiftmailer的快捷操作
*/
class Mail
{
    /**
    *发送网页邮件
    * @author cuibo weiai525@outlook.com at 2016-07-20 11:01:45
    * @param $view string 对照view()参数使用
    * @param $data array 对照view()参数使用
    * @param $message array 发送人相关信息
     */
    public static function send($view, $data,$message)
    {
        $config = config('mail');

        $transport = Swift_SmtpTransport::newInstance($config['host'], $config['port'])
                            ->setUsername($config['username'])
                            ->setPassword($config['password'])
                            ->setEncryption($config['encryption']);
        $mailer =Swift_Mailer::newInstance($transport);
        $viewObj = new View();
        $message = Swift_Message::newInstance()
                            ->setSubject($message['subject']) //创建邮件信息的主题，即发送标题 注意：Swift_Message::newInstance() 后面没有分号
                            ->setFrom($message['from']['address'],$message['from']['name']) //谁发送的 设置发送人及昵称
                            ->setTo($message['to']) //发给谁 设置接收邮件人的列表 注意：本句话结束没有分号
                            ->setBody($viewObj->fetch($view,$data), 'text/html', 'utf-8'); //邮件发送的内容 注意：当一切都设置完毕了以后，最好加上分号结束
        $mailer->send($message);
    }
    /*
    *发送纯文本邮件
     */
    public static function raw($text, $message)
    {
        $config = config('mail');
        $transport = Swift_SmtpTransport::newInstance($config['host'], $config['port'])
                            ->setUsername($config['username'])
                            ->setPassword($config['password'])
                            ->setEncryption($config['encryption']);
        $mailer =Swift_Mailer::newInstance($transport);
        $message = Swift_Message::newInstance()
                            ->setSubject($message['subject']) //创建邮件信息的主题，即发送标题 注意：Swift_Message::newInstance() 后面没有分号
                            ->setFrom($message['from']['address'],$message['from']['name']) //谁发送的 设置发送人及昵称
                            ->setTo($message['to']) //发给谁 设置接收邮件人的列表 注意：本句话结束没有分号
                            ->setBody($text); //邮件发送的内容 注意：当一切都设置完毕了以后，最好加上分号结束
        $mailer->send($message);
    }
}
