TianYou Market 0.2.0-dev32
=================

本项目目前搭载在251服务器上 [点此预览](http://202.202.240.251/market/)

----
天佑二手货本着程序轻量化、优质的用户体验、功能定位专一为理念，为交大学子提供一个二手货交易的信息平台。
> 客户端运行环境最低浏览器要求为 IE10

----
## 参与开发

注册并登录 git@osc 帐号， fork 本项目并进行改动。

----
## 版权信息

TianYou Market 遵循 GPL v2 协议

Copyright © 2016 [TianYou Network Studio.](http://ty.cqjtu.edu.cn/about/) All Rights Reserved.